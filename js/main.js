function openPopup(popup) {
  popup.classList.add("modal-content-view");
  animateElement(popup, "modal-content-view-intro");
}

function closePopup() {
  var popup = document.querySelector(".modal-content-view");
  if (popup) {
    popup.classList.remove("modal-content-view");
  }
}

function closeHandler(event) {
  event.preventDefault();
  closePopup();
}

function animateElement(element, className) {
  element.classList.add(className);
  element.addEventListener("animationend", function() {
    element.classList.remove(className);
  });
};

window.addEventListener("keydown", function(event) {
  if (event.keyCode == 27) {
    closePopup();
  }
});

// CONTACT FORM
var contactPopup = document.querySelector(".modal-contact-form");
if (contactPopup) {
  var contactPopupOpen = document.querySelector(".company-form-btn");
  var contactForm = contactPopup.querySelector("form");
  var nameInput = contactForm.querySelector("input[name=user-name]");
  var emailInput = contactForm.querySelector("input[name=user-email]");
  var textInput = contactForm.querySelector("textarea[name=modal-comment]");
  var contactPopupClose = contactPopup.querySelector(".close-btn");
  var contactPopupCancel = contactPopup.querySelector(".secondary-btn");

  contactPopupOpen.addEventListener("click", function(event) {
    event.preventDefault();
    openPopup(contactPopup);

// чтобы данные перезаписывались и показывались при активировании формы без перезагрузки страницы:
    var storageName = localStorage.getItem("nameInput");
    var storageEmail = localStorage.getItem("emailInput");

    nameInput.value = storageName;
    emailInput.value = storageEmail;

    var inputToFocus = nameInput;

    if (storageName && storageEmail) {
      inputToFocus = textInput;
    } else if (storageName) {
      inputToFocus = emailInput;
    }

    setTimeout(function() {
      inputToFocus.focus()
    }, 650);
  });

  function saveInput() {
    localStorage.setItem("nameInput", nameInput.value);
    localStorage.setItem("emailInput", emailInput.value);
  };

  function contactFormCloseHandler(event) {
    saveInput();
    closeHandler(event);
  };

  contactPopupClose.addEventListener("click", contactFormCloseHandler);
  contactPopupCancel.addEventListener("click", contactFormCloseHandler);

  contactForm.addEventListener("submit", function(event) {
    saveInput();
    if (!(nameInput.value && emailInput.value)) {
      event.preventDefault();
      animateElement(contactPopup, "modal-error");
    }
  });
}

// MAP
var mapPopup = document.querySelector(".modal-map");
if (mapPopup) {
  var mapOpen = document.querySelector(".company-contacts-map");
  var mapClose = mapPopup.querySelector(".close-btn");

  mapOpen.addEventListener("click", function(event) {
    event.preventDefault();
    openPopup(mapPopup);
  });

  mapClose.addEventListener("click", closeHandler);
}

// BUY BUTTONS
var buyButtons = document.querySelectorAll(".buy");
var okPopup = document.querySelector(".modal-ok");
var okClose = okPopup.querySelector(".close-btn");
var okCancel = okPopup.querySelector(".secondary-btn");
var shoppingCart = document.querySelector(".shopping-cart");
var userOrder = document.querySelector(".user-block-order");
var counter = shoppingCart.lastElementChild.innerHTML;


for (var i = 0; i < buyButtons.length; i++) {
  var buy = buyButtons.item(i);
  buy.addEventListener("click", function(event) {
    event.preventDefault();
    openPopup(okPopup);
    counter++;
    shoppingCart.lastElementChild.innerHTML = counter;
    shoppingCart.style.backgroundColor = "#ee3643";
    userOrder.style.display = "";
  });
}

okClose.addEventListener("click", closeHandler);
okCancel.addEventListener("click", closeHandler);

// CARUSEL
var catalogGallery = document.querySelector(".catalog-gallery");
if (catalogGallery) {

  var catalogGallerySlides = catalogGallery.querySelectorAll(".catalog-gallery-slide");
  var galleryPagination = catalogGallery.querySelector(".gallery-pagination")
  var galleryPaginationDots = galleryPagination.querySelectorAll("li");
  galleryPaginationDots = Array.prototype.slice.call(galleryPaginationDots);


  var galleryCurrentSlideIndex = 0;

  function getNextIndex() {
    var current = galleryCurrentSlideIndex;
    var max = catalogGallerySlides.length - 1;
    current++;
    if (current > max) current = 0;
    return current;
  };

  function getPrevIndex() {
    var current = galleryCurrentSlideIndex;
    var max = catalogGallerySlides.length - 1;
    current--;
    if (current < 0) current = max;
    return current;
  };

  function moveRight(event) {
    event.preventDefault(); // отменяем переход по ссылке по умолчанию
    showSlide(getNextIndex()); // показать слайд по следующему индексу
  };

  function moveLeft(event) {
    event.preventDefault();
    showSlide(getPrevIndex());
  };

  function showSlide(nextIndex) {
    var currentIndex = galleryCurrentSlideIndex;
    // прячем текущий слайд
    catalogGallerySlides[currentIndex].classList.remove("current");
    galleryPaginationDots[currentIndex].classList.remove("gallery-pagination-active");

    // показываем следующий слайд
    catalogGallerySlides[nextIndex].classList.add("current");
    galleryPaginationDots[nextIndex].classList.add("gallery-pagination-active");

    galleryCurrentSlideIndex = nextIndex;
  };

  // CARUSEL PAGINATION
  galleryPagination.onclick = function(event) {
    var target = event.target;
    var index = galleryPaginationDots.indexOf(target);
    if (index == -1) return;
    showSlide(index);
  };
  catalogGallery.querySelector(".gallery-arrow-left").addEventListener("click", moveLeft);
  catalogGallery.querySelector(".gallery-arrow-right").addEventListener("click", moveRight);
};
//END OF CARUSEL


// SERVICES-TABS
var servicesList = document.querySelector(".services-list");

if (servicesList) {
  var servicesListItems = servicesList.querySelectorAll("li a");
  servicesListItems = Array.prototype.slice.call(servicesListItems);
  var services = document.querySelectorAll(".services-description");

  var currentIndex = 0; // по умолчанию класс service-active стоит у первого li
  servicesList.onclick = function(event) {
    event.preventDefault();
    var target = event.target;
    var nextIndex = servicesListItems.indexOf(target);
    var currentTab = servicesListItems[currentIndex].closest("li"); // <- то, где сейчас стоит класс service-active
    // closest("li") - так как класс стоит не у ссылки, а у li
    var nextTab = servicesListItems[nextIndex].closest("li");
    if (nextIndex == -1) return;

    currentTab.classList.remove("service-active");
    services[currentIndex].classList.remove("services-description-current");
    nextTab.classList.add("service-active");
    services[nextIndex].classList.add("services-description-current");

    currentIndex = nextIndex;
  };
};

