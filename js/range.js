// RANGE
var rangeControls = document.querySelector(".range-controls");
if (rangeControls) {
  var scale = rangeControls.querySelector(".scale");
  var scaleBar = rangeControls.querySelector(".bar");
  var minToggle = rangeControls.querySelector(".min-toggle");
  var maxToggle = rangeControls.querySelector(".max-toggle");

  var minPrice = document.querySelector(".min-price");
  var maxPrice = document.querySelector(".max-price");

  minPrice.setAttribute("disabled", "disabled");
  maxPrice.setAttribute("disabled", "disabled");

  function updatePriceInputs(minPercent, maxPercent) {
  var minPriceValue = Math.round(maxValue * minPercent / 100);
  minPrice.setAttribute("value", minPriceValue);

  var maxPriceValue = Math.round(maxValue * maxPercent / 100);
  maxPrice.setAttribute("value", maxPriceValue);
  }

  function updateBar() {
  var min = parseInt(minToggle.style.left);
  var max = parseInt(maxToggle.style.left);

  scaleBar.style.left = min + "%";
  scaleBar.style.right = (100 - max) + "%";

  updatePriceInputs(min, max);
  };

  var maxValue = maxPrice.getAttribute("value") * 100 / parseInt(maxToggle.style.left); //максимально доступное значение, которое можно выбрать с помощью слайдера
  updateBar();



  rangeControls.onmousedown = function(event) {
    event.preventDefault(); // prevent text selection
    var target = event.target;
    // event.clientX - точка, на которую кликнули от края ОКНА
    // minToggle.getBoundingClientRect().left - расстояние от окна браузера до toggle
    // вычисляем расстояние от края toggle до места, где на него кликнули, для компенсации этого смещения (иначе toggle хватался бы за левый край)

    if (target != minToggle && target != maxToggle) return;
    var shiftX = event.clientX - target.getBoundingClientRect().left - target.offsetWidth/2;

    document.onmousemove = function(event) {
      event.preventDefault();
      var x = percent(event.pageX - getOffset(scale).left - shiftX); //процентное положение toggle на шкале

      if (target == minToggle) {
        var endX = percent(getLeft(maxToggle) - target.offsetWidth);
        if (x < 0) x = 0;
        if (x > endX) x = endX;
      } else {
        var endX = percent(getLeft(minToggle) + target.offsetWidth);
        if (x < endX) x = endX;
        if (x > 100) x = 100;
      }

      target.style.left = x + "%";

      updateBar();
    };

    document.onmouseup = function() {
      document.onmousemove = null;
      document.onmouseup = null;
    }
  };

  function percent(value) {
    return value * 100 / scale.offsetWidth;
  };



  function getLeft(elem) {
    return parseInt(getComputedStyle(elem).left);
  };

  function getOffset(el) { //вычисляет смещение относительно начала документа
    var x = 0;
    var y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        x += el.offsetLeft;
        y += el.offsetTop;
        el = el.offsetParent;
    }
    return { top: y, left: x };
  }
};